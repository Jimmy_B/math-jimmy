<?php

namespace Tests\Feature;

use Tests\TestCase;

class ConversionsControllerTest extends TestCase {

    public function testGetViewTemperature()
    {
        // J'initialise les paramètres nécessaires à l'appel de la vue
        $temperature = 0;
        $uniteInitiale = '°C';
        $unite = '°C';

        // J'appelle la vue
        $response = $this->get('/temperature',
            ['temperatureHTML' => $temperature, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);
        // Je récupère le contenu HTML
        $content = $response->content();
        // Je vérifie que le contenu contient bien ma balise h2 avec mon titre
        $this->assertTrue(str_contains($content, '<h2 id="title-page">Température</h2>'));
        /* OU
        $view = $this->view('/temperature',
            ['temperatureHTML' => $temperature, 'uniteInitiale' => $uniteInitiale,
            'unite' => $unite, 'resultHTML' => '']);
        $view->assertSeeText('Température');*/
    }

    public function testPostCalcTemperature()
    {
        // J'initialise les éléments du formulaire dont j'ai besoin
        // pour appeler la méthode POST qui convertie les températures
        $temperature = 5;
        $uniteInitiale = '°F';
        $unite = '°C';
        $response = $this->call('POST', '/temperature',
            ['temperatureVal' => $temperature, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);

        //Test $resultHTML
        $expected = '5°F équivaut à -15°C.';
        // viewData permet de récupérer une donnée renvoyée par le biais de sa clef
        // eq. {{resultHTML}} côté HTML
        $result = $response->viewData('resultHTML');

        //Test toutes les données renvoyées
        $this->assertSame($expected, $result);
        $this->assertSame($temperature, $response->viewData('temperatureHTML'));
        $this->assertSame($uniteInitiale, $response->viewData('uniteInitiale'));
        $this->assertSame($unite, $response->viewData('unite'));
    }

    public function testGetViewDistance()
    {
        $distance = 0;
        $uniteInitiale = 'km';
        $unite = 'm';

        $response = $this->get('/distance',
            ['distanceHTML' => $distance, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);
        $content = $response->content();
        $this->assertTrue(str_contains($content, '<h2 id="title-page">Distance</h2>'));
    }
    public function testPostCalcDistanceKmToM()
    {
        $distance = 5;
        $uniteInitiale = 'km';
        $unite = 'm';
        $response = $this->call('POST', '/distance',
            ['distanceVal' => $distance, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);

        $expected = '5km équivaut à 5000m.';
        $result = $response->viewData('resultHTML');

        $this->assertSame($expected, $result);
        $this->assertSame($distance, $response->viewData('distanceHTML'));
        $this->assertSame($uniteInitiale, $response->viewData('uniteInitiale'));
        $this->assertSame($unite, $response->viewData('unite'));
    }
    public function testPostCalcDistanceKmToMm()
    {
        $distance = 5;
        $uniteInitiale = 'km';
        $unite = 'mm';
        $response = $this->call('POST', '/distance',
            ['distanceVal' => $distance, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);

        $expected = '5km équivaut à 5000000mm.';
        $result = $response->viewData('resultHTML');

        $this->assertSame($expected, $result);
        $this->assertSame($distance, $response->viewData('distanceHTML'));
        $this->assertSame($uniteInitiale, $response->viewData('uniteInitiale'));
        $this->assertSame($unite, $response->viewData('unite'));
    }
    public function testPostCalcDistanceHmToKm()
    {
        $distance = 5;
        $uniteInitiale = 'hm';
        $unite = 'km';
        $response = $this->call('POST', '/distance',
            ['distanceVal' => $distance, 'uniteInitiale' => $uniteInitiale,
                'unite' => $unite, 'resultHTML' => '']);

        $expected = '5hm équivaut à 50km.';
        $result = $response->viewData('resultHTML');

        $this->assertSame($expected, $result);
        $this->assertSame($distance, $response->viewData('distanceHTML'));
        $this->assertSame($uniteInitiale, $response->viewData('uniteInitiale'));
        $this->assertSame($unite, $response->viewData('unite'));
    }
}
