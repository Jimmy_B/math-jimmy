<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controllers\Calculs\Conversions;

class ConversionsTest extends TestCase
{
    public function testConvertTempCToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°C";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 41;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToC()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°C";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = -15;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 5;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistance(){
        $distance = 5;
        $uniteInitiale = "km";
        $unite = "m";

        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        $expected = 5000;

        $this->assertSame($expected, $resultat);
    }
}
