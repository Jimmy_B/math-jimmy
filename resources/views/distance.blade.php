<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Qualité de code</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <link rel="stylesheet" media="all" href="css/style.css">
    <link rel="stylesheet" media="all" href="css/custom.css">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<header id="header">
    <div class="container">
        <a href="/" id="logo" title="HarrisonHighSchool">Qualité de code</a>
        <div class="menu-trigger"></div>
        <nav id="menu">
            <ul>
                <li><a href="/temperature" id="link-temperature">Température</a></li>
                <li><a href="/distance" id="link-distance">Distance</a></li>
                <li><a href="/airerect">Aire<br/>d'un rectangle</a></li>
                <li><a href="#"></a></li>
            </ul>
            <ul>
                <li><a href="/airetriangle">Aire<br/>d'un triangle</a></li>
                <li><a href="/perimrect">Périmètre<br/>d'un rectangle</a></li>
                <li><a href="/perimtriangle">Périmètre<br/>d'un triangle</a></li>
            </ul>
        </nav>
        <!-- / navigation -->
    </div>
    <!-- / container -->

</header>
<!-- / header -->
<div class="slider">
    <ul class="bxslider">
        <li>
            <div class="container">
                <div class="info">
                    <h2 id="title-page">Distance</h2>
                    <!--a href="#">Check out our new programs</a-->
                </div>
            </div>
            <!-- / content -->
        </li>
    </ul>
    <div class="bg-bottom"></div>
</div>

<section class="posts">
    <div class="container">
        <article>
            <div class="pic"><img width="121" src="images/2.png" alt=""></div>
            <div class="info">
                <h3>Conversion des distances</h3>
                <p class="border-blue">Grâce à cet outil, vous allez pouvoir convertir rapidement des distances d'une unité à l'autre.
                </p>
            </div>
        </article>
        <article>
            <div class="info">
                <form action="/distance" method="post">
                    @csrf
                    <p class="calc-form-distance">
                        <span class="align-content-flex">Je souhaite convertir
                            <input type="number" step="0.01" id="distanceVal" name="distanceVal"
                                                    class="input-margin-left" value={{$distanceHTML}}>
                        <select id="uniteInitiale" name="uniteInitiale" class="input-temp">
                           <!-- km, hm, dam, m, dm, cm, mm-->
                            <option value="km">km</option>
                            <option value="hm">hm</option>
                            <option value="dam">dam</option>
                            <option value="m">m</option>
                            <option value="dm">dm</option>
                            <option value="cm">cm</option>
                            <option value="mm">mm</option>
                        </select>
                        <br/>
                        en
                        <select id="unite" name="unite" class="input-temp">
                            <!-- km, hm, dam, m, dm, cm, mm-->
                            <option value="km">km</option>
                            <option value="hm">hm</option>
                            <option value="dam">dam</option>
                            <option value="m">m</option>
                            <option value="dm">dm</option>
                            <option value="cm">cm</option>
                            <option value="mm">mm</option>
                        </select>
                        </span>
                        <button id="click-result" class="btn btn-primary" type="submit">OK !</button>
                    </p>
                    <p id="resultat_distance" value="{{$resultHTML}}">{{$resultHTML}}</p>
                </form>
            </div>
        </article>
    </div>
    <!-- / container -->
</section>

<div class="container">
    <a href="#fancy" class="info-request">
			<span class="holder">
				<span class="title">Besoin d'informations</span>
				<span class="text">Vous avez des questions ? N'hésitez pas à nous les poser via le formulaire.</span>
			</span>
        <span class="arrow"></span>
    </a>
</div>

<footer id="footer">
    <div class="container">
        <section>
            <article class="col-1">
                <h3>Contact</h3>
                <ul>
                    <li class="address"><a href="#">EGUZON CHANTOME</a></li>
                    <li class="mail"><a href="#">mdagniau@almeri.fr</a></li>
                    <li class="phone last"><a href="#">00 00 00 00 00</a></li>
                </ul>
            </article>
            <article class="col-2">
                <h3>Forum topics</h3>
                <ul>
                </ul>
            </article>
            <article class="col-3">
                <h3>Social Media</h3>
                <p>Retrouvez-nous sur les réseaux sociaux.</p>
                <ul>
                    <li class="facebook"><a href="#">Facebook</a></li>
                    <li class="google-plus"><a href="#">Google+</a></li>
                    <li class="twitter"><a href="#">Twitter</a></li>
                    <li class="pinterest"><a href="#">Pinterest</a></li>
                </ul>
            </article>
            <article class="col-4">
                <h3>Newsletter</h3>
                <p>Laissez-nous votre adresse afin d'être au courant de toutes les nouveautés.</p>
                <form action="#">
                    <input placeholder="Email address..." type="text">
                    <button type="submit">Subscribe</button>
                </form>
                <ul>
                    <li><a href="#"></a></li>
                </ul>
            </article>
        </section>
        <p class="copy">Copyright 2014 Harrison High School. Designed by <a href="http://www.vandelaydesign.com/"
                                                                            title="Designed by Vandelay Design"
                                                                            target="_blank">Vandelay Design</a>. All
            rights reserved.</p>
    </div>
    <!-- / container -->
</footer>
<!-- / footer -->

<div id="fancy">
    <h2>Besoin d'informations</h2>
    <form action="#">
        <div class="left">
            <fieldset class="mail"><input placeholder="Adresse e-mail..." type="text"></fieldset>
            <fieldset class="name"><input placeholder="Nom..." type="text"></fieldset>
            <fieldset class="subject"><select>
                    <option>HTML</option>
                    <option>Laravel</option>
                    <option>Gitlab</option>
                </select></fieldset>
        </div>
        <div class="right">
            <fieldset class="question"><textarea placeholder="Question..."></textarea></fieldset>
        </div>
        <div class="btn-holder">
            <button class="btn blue" type="submit">Envoyer</button>
        </div>
    </form>
</div>
<script>
    var uniteInitiale = @json($uniteInitiale);
    if(uniteInitiale != ""){
        (document.getElementById("uniteInitiale")).value = uniteInitiale;
    }
    var unite = @json($unite);
    if(unite != ""){
        (document.getElementById("unite")).value = unite;
    }
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script>window.jQuery || document.write("<script src='js/jquery-1.11.1.min.js'>\x3C/script>")</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>
