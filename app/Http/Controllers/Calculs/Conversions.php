<?php

namespace App\Http\Controllers\Calculs;

class Conversions
{
    public static function convertTemp($temperature, $uniteInitiale, $unite){
        if($uniteInitiale == $unite){
            $res = $temperature;
        }
        else if($uniteInitiale == "°C"){
            $res = $temperature *9/5 + 32;
        }
        else{
            $res = ($temperature - 32) * 5/9;
        }

        return $res;
    }

    public static function convertDistance($distance, $uniteInitiale, $unite){
        if($uniteInitiale == $unite){
            $res = $distance;
            return $res;
        }else{
            switch ($uniteInitiale == "km") {
                case $unite == "hm":
                    $res = $distance * 10;
                    return $res;
                case $unite == "dam":
                    $res = $distance * 100;
                    return $res;
                case $unite == "m":
                    $res = $distance * 1000;
                    return $res;
                case $unite == "dm":
                    $res = $distance * 10000;
                    return $res;
                case $unite == "cm":
                    $res = $distance * 100000;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 1000000;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "hm") {
                case $unite == "km":
                    $res = $distance / 10;
                    return $res;
                case $unite == "dam":
                    $res = $distance * 10;
                    return $res;
                case $unite == "m":
                    $res = $distance * 100;
                    return $res;
                case $unite == "dm":
                    $res = $distance * 1000;
                    return $res;
                case $unite == "cm":
                    $res = $distance * 10000;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 100000;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "dam") {
                case $unite == "km":
                    $res = $distance / 100;
                    return $res;
                case $unite == "hm":
                    $res = $distance / 10;
                    return $res;
                case $unite == "m":
                    $res = $distance * 10;
                    return $res;
                case $unite == "dm":
                    $res = $distance * 100;
                    return $res;
                case $unite == "cm":
                    $res = $distance * 1000;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 10000;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "m") {
                case $unite == "km":
                    $res = $distance / 1000;
                    return $res;
                case $unite == "hm":
                    $res = $distance / 100;
                    return $res;
                case $unite == "dam":
                    $res = $distance / 10;
                    return $res;
                case $unite == "dm":
                    $res = $distance * 10;
                    return $res;
                case $unite == "cm":
                    $res = $distance * 100;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 1000;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "dm") {
                case $unite == "km":
                    $res = $distance / 10000;
                    return $res;
                case $unite == "hm":
                    $res = $distance / 1000;
                    return $res;
                case $unite == "dam":
                    $res = $distance / 100;
                    return $res;
                case $unite == "m":
                    $res = $distance / 10;
                    return $res;
                case $unite == "cm":
                    $res = $distance * 10;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 100;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "cm") {
                case $unite == "km":
                    $res = $distance / 100000;
                    return $res;
                case $unite == "hm":
                    $res = $distance / 10000;
                    return $res;
                case $unite == "dam":
                    $res = $distance / 1000;
                    return $res;
                case $unite == "m":
                    $res = $distance / 100;
                    return $res;
                case $unite == "dm":
                    $res = $distance / 10;
                    return $res;
                case $unite == "mm":
                    $res = $distance * 10;
                    return $res;
                default:
                    echo "Erreur";
            }
            switch ($uniteInitiale == "mm") {
                case $unite == "km":
                    $res = $distance / 1000000;
                    return $res;
                case $unite == "hm":
                    $res = $distance / 100000;
                    return $res;
                case $unite == "dam":
                    $res = $distance / 10000;
                    return $res;
                case $unite == "m":
                    $res = $distance / 1000;
                    return $res;
                case $unite == "dm":
                    $res = $distance / 100;
                    return $res;
                case $unite == "cm":
                    $res = $distance / 10;
                    return $res;
                default:
                    echo "Erreur";
            }
        }
    }

}
