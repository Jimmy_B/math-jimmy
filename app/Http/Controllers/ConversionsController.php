<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Calculs\Conversions;

class ConversionsController extends Controller
{
    public function getViewTemperature()
    {
        // J'initialise les valeurs par défaut qui seront affichées dans la vue initiale
        $temperature = 0;
        $result = "";

        // Je renvoie la vue se nommant temperature.blade.php
        // Avec les informations suivantes temperatureHTML (variable côté HTML)
        // prendra la valeur de $temperature
        // & resultHTML prendra la valeur de $result
        return view('temperature', [
            'temperatureHTML' => $temperature,
            'uniteInitiale' => "°C",
            'unite' => "°C",
            'resultHTML' => $result,
        ]);
    }

    public function calcTemperature(){
        // On récupère les éléments via l'attribut name dans le formulaire
        $temperature = request('temperatureVal');
        $uniteInitiale = request('uniteInitiale');
        $unite = request('unite');
        //A vous de créer la méthode dans la classe Calculs\Conversions
        $res = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //On rédige une phrase
        $result = $temperature.$uniteInitiale." équivaut à ".$res.$unite.".";

        // Je renvoie la vue se nommant temperature.blade.php
        // Avec les informations suivantes
        return view('temperature', [
            'temperatureHTML' => $temperature,
            'uniteInitiale' => $uniteInitiale,
            'unite' => $unite,
            'resultHTML' => $result,
        ]);
    }

    public function getViewDistance()
    {
        // J'initialise les valeurs par défaut qui seront affichées dans la vue initiale
        $distance = 0;
        $result = "";

        // Je renvoie la vue se nommant distance.blade.php
        // Avec les informations suivantes distanceHTML (variable côté HTML)
        // prendra la valeur de $distance
        // & resultHTML prendra la valeur de $result
        return view('distance', [
            'distanceHTML' => $distance,
            'uniteInitiale' => "km",
            'unite' => "km",
            'resultHTML' => $result,
        ]);
    }

    public function calcDistance(){
        // On récupère les éléments via l'attribut name dans le formulaire
        $distance = request('distanceVal');
        $uniteInitiale = request('uniteInitiale');
        $unite = request('unite');
        //A vous de créer la méthode dans la classe Calculs\Conversions
        $res = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //On rédige une phrase
        $result = $distance.$uniteInitiale." équivaut à ".$res.$unite.".";

        // Je renvoie la vue se nommant distance.blade.php
        // Avec les informations suivantes
        return view('distance', [
            'distanceHTML' => $distance,
            'uniteInitiale' => $uniteInitiale,
            'unite' => $unite,
            'resultHTML' => $result,
        ]);
    }
}
